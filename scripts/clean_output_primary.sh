#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: bash script to clean primary clustering output

export LC_NUMERIC="en_US.UTF-8"

SCRIPT_PATH=${0%/*}

# include error handling functions
[[ $(type -t die) == function ]] || source ${SCRIPT_PATH}/aux/error_handling.sh

# parser -> $INPUT, $OUTDIR, $NUM_BLOCKS 
source ${SCRIPT_PATH}/aux/arg_parser.sh

# check input folder and files
prycl_folder=${INPUT}
[ -d "$prycl_folder" ] || die "Primary cluster folder not found, '$prycl_folder'"
for ((i=1;i<=NUM_BLOCKS;++i)); 
do
    prycl=${prycl_folder}"/"${i}".cl"
    [ -s "$prycl" ] || die "Missing primary cluster file, '$prycl'"
done

# outputs
mkdir -p ${OUTDIR} || die "Error creating output directory '${OUTDIR}'"

# cycle over NUM_BLOCKS 
for ((i=1;i<=NUM_BLOCKS;++i)); do
   awk '$4>-1{print $1, $4, $6, $7, $8}'  ${prycl_folder}"/"${i}.cl > ${OUTDIR}"/C"${i}.cl
done