#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: script to run blast search. 


export LC_NUMERIC="en_US.UTF-8"


SCRIPT_PATH=${0%/*}
BIN_PATH=${SCRIPT_PATH%/*}/bin
[ "." == "$SCRIPT_PATH" ] && BIN_PATH=.${BIN_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${SCRIPT_PATH}/aux/error_handling.sh

# parser -> $INPUT, $OUTDIR, $NUM_BLOCKS 
source ${SCRIPT_PATH}/aux/arg_parser.sh

# check variables
fastas_folder=${INPUT}

# fastas
[ -d "$fastas_folder" ] || die "Fastas folder not found, '$fastas_folder'"
for ((i=1;i<=NUM_BLOCKS;++i)); 
do
    fasta=${fastas_folder}"/"${i}".fasta"
    [ -s "$fasta" ] || die "Missing fasta file, '$fasta'"
done

# outputs
mkdir -p ${OUTDIR} || die "Error creating output directory '${OUTDIR}'"

align_folder=${OUTDIR}/alignments
mkdir -p ${align_folder} || die "Error creating alignment directory '${align_folder}'"

# check if makeblastdb & blastp are installed
if ! command -v makeblastdb &> /dev/null
then
    die "makeblastdb could not be found"
else
    echo "OK, makeblastdb found"
fi

if ! command -v blastp &> /dev/null
then
    die "blastp could not be found"
else
    echo "OK, blastp found"
fi

# Build blast dataset (using global file)
echo "Runnig makeblastdb"
for ((i=1;i<=NUM_BLOCKS;++i)); do
    cat ${fastas_folder}/${i}.fasta >> ${fastas_folder}/tmp_numeric
done
makeblastdb -in ${fastas_folder}/tmp_numeric -parse_seqids -out ${OUTDIR}"/blastdb" -dbtype prot
rm ${fastas_folder}/tmp_numeric

# Run blast search(es)
echo "Runnig blastp"
for ((i=1;i<=NUM_BLOCKS;++i)); do
    blastp -query ${fastas_folder}/${i}".fasta" -db ${OUTDIR}"/blastdb" \
        -evalue 0.1 -max_target_seqs 20000 -out ${align_folder}/${i}".blasted" \
        -outfmt "6 qseqid sseqid qstart qend sstart send qlen slen length pident evalue score" \
        -num_threads 4 ; 
done

