#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: unittest of ./primarycl_command.sh 

# export LC_NUMERIC="en_US.UTF-8"

TEST_PATH=${0%/*}
SCRIPT_PATH=${TEST_PATH%/*}/scripts
[ "." == "$TEST_PATH" ] && SCRIPT_PATH=.${SCRIPT_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${TEST_PATH}/aux/error_handling.sh

# parser -> $LOGIFLE, $DATASET, $NUM_BLOCKS 
source ${TEST_PATH}/aux/arg_parser.sh
source ${TEST_PATH}/aux/test_msgs.sh

REF_PATH=${TEST_PATH}/data/set${DATASET}
OUT_PATH=test${DATASET}_logs/primary_clusters

echo ">>> Dataset ${DATASET}: Testing primarycl_command.sh" | tee -a $LOGFILE

# run command
${SCRIPT_PATH}/primarycl_command.sh -o ${OUT_PATH} -n $NUM_BLOCKS ${REF_PATH}/alignments &>> $LOGFILE
rm ${OUT_PATH}/Icl.log
if [ $? -ne 0 ]; then
	die "Test could not be performed. Command exectution failed please check '${LOGFILE}'"
fi

EC=$NUM_BLOCKS
# check blocks (i.cl)
for ((i=1;i<=NUM_BLOCKS;++i)); do
	cmp ${OUT_PATH}/${i}.cl ${REF_PATH}/primary_clusters/${i}.cl &> ${OUT_PATH}/tt
	if [ $? -eq 0 ]; then
		passed ${i}.cl $LOGFILE
		rm ${OUT_PATH}/${i}.cl
		EC=$((EC - 1))
	else
		failed ${i}.cl $LOGFILE
		cat ${OUT_PATH}/tt >> $LOGFILE		
	fi
done

# remove tmp files
rm ${OUT_PATH}/tt

# remove if empty
[ "$(ls -A ${OUT_PATH})" ] || rm -r ${OUT_PATH}

# remove color codes from logfile
sed -i -e 's/\x1b\[[0-9;]*m//g' $LOGFILE
sed -i -e 's/^[ \t]*//' $LOGFILE

if (($EC>0)); then
	die "Failed, $EC errors found"
else
	[ "$(ls -A test${DATASET}_logs)" ] || rm -r test${DATASET}_logs
	echo "   Test passed!"
fi
