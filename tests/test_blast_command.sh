#!/bin/bash

# Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfam pipeline
# SPDX-License-Identifier:  BSD-2-Clause
# Description: unittest of ./blast_command.sh 

# export LC_NUMERIC="en_US.UTF-8"

TEST_PATH=${0%/*}
SCRIPT_PATH=${TEST_PATH%/*}/scripts
[ "." == "$TEST_PATH" ] && SCRIPT_PATH=.${SCRIPT_PATH}

# include error handling functions
[[ $(type -t die) == function ]] || source ${TEST_PATH}/aux/error_handling.sh

# parser -> $LOGIFLE, $DATASET, $NUM_BLOCKS 
source ${TEST_PATH}/aux/arg_parser.sh
source ${TEST_PATH}/aux/test_msgs.sh

REF_PATH=${TEST_PATH}/data/set${DATASET}
OUT_PATH=test${DATASET}_logs

echo ">>> Dataset ${DATASET}: Testing blast_command.sh" | tee -a $LOGFILE

# run command
${SCRIPT_PATH}/blast_command.sh  -o ${OUT_PATH} -n $NUM_BLOCKS ${REF_PATH}/fastas &>> $LOGFILE
if [ $? -ne 0 ]; then
	die "Test could not be performed. Command exectution failed please check '${LOGFILE}'"
fi

# check outputs
out_list=( blastdb.phr blastdb.pog blastdb.psd blastdb.psi blastdb.psq )

EC=$((NUM_BLOCKS + ${#out_list[@]}))

for file in ${out_list[@]}; do
    cmp ${OUT_PATH}/$file ${REF_PATH}/$file &> ${OUT_PATH}/tt
    if [ $? -eq 0 ]; then
	    passed $file $LOGFILE
		rm ${OUT_PATH}/$file
	   	EC=$((EC - 1))
    else
	   	failed $file $LOGFILE
		cat ${OUT_PATH}/tt >> $LOGFILE
	fi
done

# check blocks (i.blasted)
for ((i=1;i<=NUM_BLOCKS;++i)); do
    cmp ${OUT_PATH}/alignments/${i}.blasted ${REF_PATH}/alignments/${i}.blasted &> ${OUT_PATH}/tt
    if [ $? -eq 0 ]; then
	   	passed ${i}.blasted $LOGFILE
		rm ${OUT_PATH}/alignments/${i}.blasted
	   	EC=$((EC - 1))
    else
	   	failed ${i}.blasted $LOGFILE
		cat ${OUT_PATH}/tt >> $LOGFILE
    fi
done

# tmp file
rm ${OUT_PATH}/tt
rm ${OUT_PATH}/blastdb.pin

# remove if empty
[ "$(ls -A ${OUT_PATH}/alignments)" ] || rm -r ${OUT_PATH}/alignments

# remove color codes from logfile
sed -i -e 's/\x1b\[[0-9;]*m//g' $LOGFILE
sed -i -e 's/^[ \t]*//' $LOGFILE

if (($EC>0)); then
	die "Failed, $EC errors found"
else
	[ "$(ls -A test${DATASET}_logs)" ] || rm -r test${DATASET}_logs
	echo "   Test passed!"
fi
