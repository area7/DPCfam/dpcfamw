
// Author: DPCfam authors @ Institute for Research and Technologies (Area Science Park) & SISSA.
// Package: DPCfam pipeline
// SPDX-License-Identifier:  BSD-2-Clause
// Description: c++ program that reads dist_Icl binary output, and given a set of primary clusters outputs in ascii formats matrix entries restricted to that subset.


/******************************************************************************** 
* The output consists of 3 columns: 
* queryID*100 + center, searchID (s), search start (ss), search end (se)
******************************************************************************/

#include <fstream>
#include <iostream>
#include <unistd.h> // getopt
#include <vector>
#include <cstdint>


int main(int argc, char** argv)
{
    //-------------------------------------------------------------------------
    // Argument parser

    int opt;
    std::string inFilename, listFilename;

    while ((opt = getopt(argc, argv, "hl:")) != -1) 
    {
        switch (opt) 
        {
         case 'l':
             listFilename = optarg;
             break;
        case 'h':
            // go to default

        default: /* '?' */
            std::cerr << "Usage: \n";
            std::cerr << "\t " << argv[0] << " INPUT -l listINPUT\n\n";
            std::cerr << "\t INPUT      input (dmat binary) filename \n\n";
            std::cerr << "\t listINPUT      list input filename  - list of primary clusters to extract, ascii format, one MC per line \n\n";
            std::cerr << "Description:\n\t Reads INPUT dist_Icl binary output file, searches for mcs listed in lisiNPUT and outputs restricted matrix to terminal.\n\n";
            exit(EXIT_FAILURE);
        }
    }

    if (optind != argc - 1) 
    {
        std::cerr << "Expected single argument after options." << std::endl;
        exit(EXIT_FAILURE);
    }
    else
    {
        inFilename = argv[optind];
        std::cerr << "INPUT: " << inFilename << "listINPUT: " << listFilename << std::endl;
    }

    //Read (ascii) file containing MCs list.

    // vector to store MCs
    std::vector<uint64_t> PCs;
    // read

    uint64_t number;

    std::ifstream listfile(listFilename, std::ifstream::in);
    if (!listfile.is_open()) {
        std::cerr << "Could not open the file - '"
            << listFilename << "'" << std::endl;
        return EXIT_FAILURE;
    }

    while (listfile >> number) {
        //std::cout << number << " ";
        PCs.push_back(number);
    }
    listfile.close();

    std::cerr << "Number of PCs for which we extract submatrix: " << PCs.size() << std::endl;

    //for (int i = 0; i < MCs.size(); ++i) { std::cout << "\n at position -" << i << "- there is number " << MCs[i] << std::endl; }


    //Read Binary-------------------------------------------------------------------------
    
    std::ifstream infile (inFilename, std::ifstream::binary);
    uint64_t length = 0;
    char * buffer = NULL;
    uint32_t * pInteger = NULL;
    double * pDouble = NULL;

    // read file
    if (infile) 
    {
        // get length of file:
        infile.seekg (0, infile.end);
        length = infile.tellg();
        infile.seekg (0, infile.beg);

        buffer = new char [length];
        
        std::cerr << "Reading " << length << " characters... ";
        // read data as a block:
        infile.read (buffer,length);
        std::cerr << "all characters read successfully. \n";
        
        pInteger = (unsigned int*) buffer;
        pDouble = (double *) buffer;
    }
    else
    {
      std::cerr << "error: only " << infile.gcount() << " could be read \n";
    }
    infile.close();
    
    // each line has 3 unsigned int entries
    uint32_t lines = length/(2*sizeof(uint32_t) + sizeof(double));

    for (uint64_t i = 0; i < lines; ++i)
    {
        uint64_t pc1 = pInteger[4 * i];
        uint64_t pc2 = pInteger[4 * i + 1];
        double distance = pDouble[2 * i + 1];
        // support variable to decide wether to print or not the output line
        int toprint = 0;
        // check if pc1 and then pc2 are in the vector. add 1 once you find them, then break immediately to spare time. 
        for (size_t i = 0; i < PCs.size(); ++i) { if (pc1 == PCs[i]) { toprint++; break; }    }
        for (size_t i = 0; i < PCs.size(); ++i) { if (pc2 == PCs[i]) { toprint++; break; }    }

        // pointer arithmetic depends on the pointer type: 3 unsigned int per line || 6 unsigned short per line.
        //std::cout << pInteger[4*i] << ' ' << pInteger[4*i+1] << ' ' << pDouble[2*i + 1] <<'\n';

        //if toprint is 2 then we found both pc1 and pc2 in the list of the PCs to extract, so you print them.
        if(toprint==2)  std::cout << pc1 << ' ' << pc2 << ' ' << distance << '\n';
    }
    

    delete[] buffer;

  return 0;
}

